const express = require('express')
const { isNil } = require('ramda')
const jwt = require('jsonwebtoken');
const cors = require('cors');
const { shieldUrl } = require('../config')
const queries = require('./mongodb/queries')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const axios = require('axios')
const io = require('socket.io')();

queries.connectDb();

const app = express()
const port = 8080
const cookieName = 'shield.token'

app.use(cors({ credentials: true, origin: 'http://localhost:3000' }));
app.use(bodyParser.json())
app.use(cookieParser())

io.on('connection', (client) => {
    client.on('subscribeToTimer', (interval) => {
      console.log('client is subscribing to timer with interval ', interval);
      setInterval(() => {
        var now = new Date();
        client.emit('timer', new Date(now.getTime() - now.getTimezoneOffset() * 60000));
      }, interval);
    });
  });
  io.listen(4001);

app.post('/login', async (req, res) => {
    const data = req.body;
    var getUser = await checkAuth(data.id, data.username)
    if (getUser != false) {
        res.cookie(cookieName, generateCookie(getUser.id, getUser.firstName, getUser.isAdmin))
        res.status(200).end()
    } else {
        res.status(403).end();
    }
})

app.get('/getUser', async (req, res) => {
    if (req.cookies && req.cookies[cookieName]) {
        const user = await extractUser(req.cookies[cookieName]);
        if (user) {
            req.user = user;
            res.json(req.user)
            return;
        }
    }
    res.status(403).end();
})

const generateCookie = (id, name, admin) => {
    return (jwt.sign({
        id: id,
        userName: name,
        isAdmin: admin
    }, 'shield', { expiresIn: '1h' }))
}
const extractUser = (token) => {
    const decoded = jwt.verify(token, 'shield', function (err, decoded) {
        if (err) {
            console.log(err.message)
            return null;
        } else {
            // console.log(decoded.id + ":" + decoded.userName)
            return decoded;
        }
    });
    return decoded
}

const checkAuth = (id, name) => {
    return queries.isUserExist(id, name)
}

app.get('/getWeather', async (req, res) => {
    weather = await axios.get('http://api.openweathermap.org/data/2.5/weather?id=293703&lang=he&units=metric&appid=0530bf9233e36ae369af8e06cbff4703')
    res.json(weather.data)
})

app.get('/countDefenseByCoures', (req, res) => {
    queries.countDefenseByCoures(res)
})

// app.get('/allCourses', (req, res) => {
//     res.send(queries.getAllCourses())
// })

app.get('/allUsers', (req, res) => {
    queries.getAllUsers(res)
})

app.post('/addUser', (req, res) => {
    const data = req.body;
    queries.addUser(res, data)
})

app.post('/deleteUser', (req, res) => {
    const data = req.body;
    queries.deleteUser(res, data)
})

app.post('/updateUser', (req, res) => {
    const data = req.body;
    queries.updateUser(res, data)
})

app.get('/allCourses', (req, res) => {
    queries.getAllCourses(res)
})

app.post('/addCourse', (req, res) => {
    const data = req.body;
    queries.addCourse(res, data)
})

app.post('/addDefense', (req, res) => {
    const data = req.body;
    queries.addDefense(res, data)
})

app.post('/deleteCourse', (req, res) => {
    const data = req.body;
    queries.deleteCourse(res, data)
})

app.get('/searchCourses', (req, res) => {
    const data = req.query
    queries.searchCourses(res, data)
})

app.post('/searchDefense', (req, res) => {
    const data = req.body;
    queries.getDefenseByFilter(res, data)
})

app.get('/getCourseByID', (req, res) => {
    const data = req.query.id;
    queries.getCourseByID(res, data)
})

app.post('/updateCourse', (req, res) => {
    const data = req.body;
    queries.updateCourse(res, data)
})

app.get('/allDefenses', (req, res) => {
    queries.getAllDefenses(res)
})

app.get('/getCoursesWithAvgStudents', (req, res) => {
    queries.getCoursesWithAvgStudents(res)
})

app.get('/testFenc', (req, res) => {
    queries.testFenc(res)
})


app.get('/getDefenses', (req, res) => {
    const data = req.query.id
    queries.getDefenses(res, data)
})


app.post('/getDefenseRegisterById', (req, res) => {
    const data = req.body;
    queries.getDefenseRegisterById(res, data)
})

app.post('/getDefenseById', (req, res) => {
    const data = req.body;
    queries.getDefenseById(res, data)
})

app.post('/register', (req, res) => {
    const data = req.body;
    queries.register(res, data)
})



app.listen(port, () => console.log(`Example app listening on port ${port}!`))
const schemas = require('./schemas')
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// var User = mongoose.model('User', schemas.userSchema);
// var Course = mongoose.model('Course', schemas.courseSchema);

// Don't change- works. Ron
const Users = mongoose.model('Users', new Schema({
    id: String,
    firstName: String,
    lastName: String,
    isAdmin: Boolean,
    city: String
}), 'Users')

const Courses = mongoose.model('Courses', new Schema({
    id: Number,
    name: String,
    lecturerID: Number,
    lecturer: String,
    semester: String,
    year: String,
    minStudents: Number,
    maxStudents: Number,
    numOfStudents: Number,
    description: String
}), 'Courses')

const Defenses = mongoose.model('Defenses',new Schema({
    courseName: String,
    lecturer: String,
    semester: String,
    courseId: String,
    date: Date,
    defenseLength: Number,
    startTime: Date,
    endTime: Date, 
    description: String,
}), 'Defenses')

const DefenseRegisters = mongoose.model('DefenseRegisters',new Schema({
    defenseId: String,
    date: Date,
    hour: String,
    groupMembers: Array
}), 'DefenseRegisters')
// Don't change- works. Ron

const connectDb = () => {
    try {
        mongoose.connect('mongodb+srv://admin:Ram2020@shield-oui7j.gcp.mongodb.net/Shield', { useNewUrlParser: true, useUnifiedTopology: true });
        console.log("connected to mongo db")
    } catch (error) {
        handleError(error);
    }
}

function getAllUsers(res) {
    Users.find({}).exec(function (err, docs) { res.send(docs) })
}

function countDefenseByCoures(res) {
    // Defenses.aggregate([])
    Defenses.aggregate([
        {
          $group: {
            _id: { date: "$courseName" } ,
            count: { $sum: 1 }
          }
        }
      ])
    .exec(function (err, docs) { res.send(docs) })
}

function deleteUser(res, data) {
    Users.find({ id: data.id }).remove().exec()
    res.send();
}

function addUser(res, data) {
    var newUser = new Users({
        firstName: data.firstName,
        lastName: data.lastName,
        id: data.id,
        isAdmin: data.isAdmin,
        city: data.city
    })
    newUser.save(function (err, u) {
        if (err) return console.error(err);
        console.log(u.id + " saved to Users collection.");
    });
    res.send();
}

function updateUser(res, data) {
    console.log(data)
    var userId = { id: data.id }
    var detailsToChange = {
        firstName: data.firstName,
        lastName: data.lastName,
        city: data.city,
        isAdmin: data.isAdmin
    }

    Users.findOneAndUpdate(userId, detailsToChange, function (err, u) {
        console.log(u)
        if (err) return res.send(500, { error: err });
        res.send(200)
        console.log(u.id + " Updated in Users collection.");
    });
}

function addCourse(res, data) {
    console.log(data)
    var newCourse = new Courses({
        name: data.name,
        lecturer: data.lecturer,
        lecturerID: data.lecturerID,
        lecturer: data.lecturer,
        semester: data.semester,
        year: data.year,
        minStudents: data.minStudents,
        maxStudents: data.maxStudents,
        numOfStudents: data.numOfStudents,
        description: data.description
    })
    newCourse.save(function (err, u) {
        if (err) 
        { 
            res.status(200).end()
            return console.error(err)
        }
        else
        {
            res.status(403).end();
            console.log(u.id + " saved to Courses collection.");
        }
    });
}

function getAllCourses(res) {
    Courses.find({}).exec(function (err, docs) { res.send(docs) })
}

function deleteCourse(res, data) {
    Courses.find({ _id: data.id }).remove().exec()
}

function searchCourses(res, data) {
   console.log(data.name)
   var d = { }
   if(data.name!='')
   {
       d= {...d , name: {$regex: ".*" + data.name + ".*"}}
   }
   if(data.year!='')
   {
       d= {...d , year: {$regex: ".*" + data.year + ".*"}}
   }
   if(data.semester!='')
   {
       d= {...d , semester: {$regex: ".*" + data.semester + ".*"}}
   }
   console.log(d)

    Courses.find(d).exec(function (err, docs) { res.send(docs) })
}

function getCourseByID(res, data) {
    Courses.findById(data).exec(function (err, docs) { res.send(docs) })
}

function updateCourse(res, data) {
    console.log(data)
    console.log(data._id)
    var courseID = data._id
    var detailsToChange = {
                name: data.name,
                lecturer: data.lecturer,
                lecturerID: data.lecturerID,
                year: data.year,
                semester: data.semester,
                minStudents: data.minStudents,
                maxStudents: data.maxStudents, 
                numOfStudents: data.numOfStudents,
                description: data.description,
    }

    Courses.findByIdAndUpdate(courseID, detailsToChange, function (err, u) {
        console.log(u)
        if (err) return res.send(500, { error: err });
        res.send(200)
        console.log(u.name + " Updated in Courses collection.");
    });
}

function getAllDefenses(res) {
    Defenses.find({}).exec(function(err, docs) {
        res.send(docs) 
    })
}

function getDefenses(res, data) {
    console.log(data)
     Defenses.find({courseId: data}).exec(function (err, docs) { res.send(docs) })
 }
function getDefenseByFilter(res, data) {
    Defenses.find({
        name: {$regex :".*" + data.name + ".*"}, 
        lecturer: {$regex :".*" + data.lecturer + ".*"},
        semester: {$regex :".*" + data.lecturer + ".*"},
    }).exec(function(err, docs) {
        res.send(docs) 
    })
}

function hourString(date) {
    var hours = date.getHours()
    var mins = date.getMinutes()
    if (hours < 10)
        hours = "0" + hours
    if (mins < 10)
        mins = "0" + mins

    return hours + ":" + mins    
}

function addDefense(res, data) {
    res.send();
    console.log(data.courseId)
    Courses.findOne({_id: data.courseId}).exec(function (err, docs) {
        var newDefense = new Defenses({
            courseId: docs._id,
            courseName: docs.name,
            lecturer: docs.lecturer,
            semester: docs.semester,
            date: new Date(data.date),
            defenseLength: data.defenseLength,
            startTime: new Date(data.startTime),
            endTime: new Date(data.endTime), 
            description: "הגנה פרונטלית",
        })
        console.log(newDefense)
        newDefense.save(function (err, d) {
            if (err) return console.error(err);
            console.log(d.courseName + " saved to Defense collection.");

            var interval = new Number(d.defenseLength)
            var start = newDefense.startTime

            // var end = new Date(start)
            // end.setMinutes(end.getMinutes() + interval);

            var end = new Date(start.getTime() + interval*60000)
            console.log(end)
            while (end <= newDefense.endTime) {
                var newRegister = new DefenseRegisters({
                    defenseId: d._id,
                    date: newDefense.date,
                    hour: hourString(start),
                    groupMembers: [] 
                })
                console.log(newRegister);

                newRegister.save(function (err, dr) {
                    if (err) return console.error(err);
                    console.log(dr._id + " saved to DefenseRegister collection.");
                })

                var start = end
                var end = new Date(start.getTime() + interval*60000)

            }
        });

        

        // var newDateObj = new Date(oldDateObj.getTime() + diff*60000);

    })
}

function getDefenseRegisterById(res, data) {
    DefenseRegisters.find({defenseId: data.defenseId}).select('hour').exec(function(err, docs) {
        res.send(docs) 
    })
}

function getCoursesWithAvgStudents (res, data) {
    Courses.find({}).select('name minStudents maxStudents').exec(function(err, docs) {
        res.send(docs) 
    })
}

function testFenc (res, data) {
    var agg = [{$group:{courseName: ""}}]

    Defenses.aggregate([{$match: {}}, {$group: {}}], function(err, orders) {
          res.send(orders);
      });

      aggregate.group({ _id: "$department" });
}

function getDefenseById(res, data) {
    Defenses.findOne({_id: data.defenseId}).exec(function(err, docs) {
        res.send(docs) 
    })
}

function deleteDefense(res, data) {
    Defenses.find({ _id: data.id }).remove().exec()
}

function register(res, data) {
    console.log(data)
    var defenseRegister = { 
        defenseId: data.defenseId,  
        hour: data.hour,
    }
    var detailsToChange = {
        groupMembers: data.students
    }

    DefenseRegisters.findOneAndUpdate(defenseRegister, detailsToChange, function (err, u) {
        console.log(u)
        if (err) return res.send(500, { error: err });
        res.send(200)
        console.log(u._id + " Updated in DefenseRegister collection.");
    });
}

// You can chage from here

async function isUserExist(id, name) {
    var exist = await Users.findOne({ id: id, firstName: name })
    if (exist == null) {
        return false
    }
    return exist.toJSON();
}

function findUser(id, name) {
    return Users.findOne({ id: id, name: name })
}

// function getUserNameById(id) {
//     return Users.findOne({ id: id }).name
// }


// function bla(a, docs) {
//     console.log(docs)
// }

// function getAllCourses() {
//     return Course.find({})
// }

// function getAllDefenses() {
//     return Course.find({})
// }

module.exports = {
    getAllUsers: getAllUsers,
    connectDb: connectDb,
    Users: Users,
    addUser: addUser,
    updateUser: updateUser,
    deleteUser: deleteUser,
    addCourse: addCourse,
    getAllCourses: getAllCourses,
    deleteCourse: deleteCourse,
    getAllDefenses: getAllDefenses,
    isUserExist: isUserExist,
    searchCourses: searchCourses,
    getDefenseByFilter: getDefenseByFilter,
    addDefense: addDefense,
    getDefenseRegisterById: getDefenseRegisterById,
    getDefenseById: getDefenseById,
    register: register,
    getCourseByID: getCourseByID,
    updateCourse: updateCourse,
    getDefenses: getDefenses,
    countDefenseByCoures: countDefenseByCoures,
    getCoursesWithAvgStudents: getCoursesWithAvgStudents,
    testFenc:testFenc,
}
const userSchema = () => {

    return new Schema({
        id: number,
        name: String,
        admin: number
    })
}

const courseSchema = () => {

    return new Schema({
        id: number,
        name: String,
        lecturerId: number,
        semester: string,
        year: number,
        defenses: [{
            id: number,
            startTime: number,
            endTime: number,
            duration: number,
            minPeople: number,
            maxPeople: number,
            defenseGroups: [{
                id: number,
                time: number,
                students: [{
                    id: number
                }]
            }]
        }]
    })
}
dbSchemas:

User: {
    id: number
    name: string
    admin: number
}

Course: {
    id: number
    name: string
    lecturerId: number
    semester: number
    year: number
    defenses: [{
        id: number,
        startTime: ISO,
        endTime: ISO,
        duration: number,
        minPeople: number,
        maxPeople: number,
        groups:[{
            defenseId: number,
            time: ISO,
            students: [{
                id: number
            }]
        }]        
    }]
}